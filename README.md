# Project New RPG
## Built in
##### Build Pipeline
[![Compile](https://img.shields.io/endpoint?url=https://json.extendsclass.com/bin/d9c79e5bf535)](https://gitlab.com/gtion-production/project-new-rpg/-/pipelines) [![Build CI](https://img.shields.io/endpoint?url=https://json.extendsclass.com/bin/c58770a4c6de)](https://gitlab.com/gtion-production/project-new-rpg/-/pipeline_schedules) [![Unity](https://img.shields.io/endpoint?url=https://json.extendsclass.com/bin/440d9fb09718)](unityhub://2020.3.4f1/0abb6314276a) [![Version](https://img.shields.io/endpoint?url=https://json.extendsclass.com/bin/856d21f10c0f
)](https://gitlab.com/gtion-production/project-new-rpg/-/pipelines)

##### Framework
[![Unity](https://img.shields.io/badge/Unity-100000?style=for-the-badge&logo=unity&logoColor=white)](https://unity.com/) [![CSharp](https://img.shields.io/badge/C%23-239120?style=for-the-badge&logo=c-sharp&logoColor=white)](https://docs.microsoft.com/en-us/dotnet/csharp/) [![VStudio](https://img.shields.io/badge/Visual_Studio_2019-5C2D91?style=for-the-badge&logo=visual%20studio&logoColor=white)](https://docs.microsoft.com/en-us/dotnet/csharp/)


##### Release OS
[![Windows](https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white)](https://gtion.itch.io/infinite-tower-rpg) [![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black)](https://gtion.itch.io/infinite-tower-rpg) [![Android](https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=android&logoColor=white)](https://gtion.itch.io/infinite-tower-rpg) 

##### Store Pages
[![Google Play](https://img.shields.io/badge/Google_Play-414141?style=for-the-badge&logo=google-play&logoColor=white)](https://gtion.itch.io/infinite-tower-rpg) [![Steam](https://img.shields.io/badge/Steam-000000?style=for-the-badge&logo=steam&logoColor=white)](https://gtion.itch.io/infinite-tower-rpg) [![ItchIO](https://img.shields.io/badge/Itch.io-FA5C5C?style=for-the-badge&logo=itch.io&logoColor=white)](https://gtion.itch.io/infinite-tower-rpg)

## Task
##### Scrum Board
Task masing masing role dapat diakses dibawah ini
| Role | Link |
| ------ | ------ |
| Game Programming | [GP Board](https://gitlab.com/gtion-production/project-new-rpg/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=game%20programming) |
| Game Arts | [GA Board](https://gitlab.com/gtion-production/project-new-rpg/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=game%20art) |
| Game Design | [GD Board](https://gitlab.com/gtion-production/project-new-rpg/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=game%20design) |
| Game Animation | [Anim Board](https://gitlab.com/gtion-production/project-new-rpg/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=game%20animation) |
| Game Audio | [GC Board](https://gitlab.com/gtion-production/project-new-rpg/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=game%20audio) |
| Game Technology | [Tech Board](https://gitlab.com/gtion-production/project-new-rpg/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=game%20technology) |
| Game Story | [Plot Board](https://gitlab.com/gtion-production/project-new-rpg/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=game%20story) |
| Game Advertise | [Ads Board](https://gitlab.com/gtion-production/project-new-rpg/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=game%20advertisement) |

##### Milestone
Our progress can be viewed on the milestones bellow
[See Milestones](https://gitlab.com/gtion-production/project-new-rpg/-/milestones)

## Reference
##### Similar Games
Game similar reference below
- [Battle Chaser:Night War](https://www.youtube.com/watch?v=-LibVd3-KYg&t=508s)
- [Toram Online](https://www.youtube.com/watch?v=JgJC5uhzOlM)
- [Xenoblade Chronicles](https://www.youtube.com/watch?v=mX-vU5_WLIs)
- [Sword Art Online](https://www.youtube.com/watch?v=6ohYYtxfDCg)
- [The Tutorial is Too Hard](https://mangatx.com/manga/the-tutorial-is-too-hard/comment-page-1/)
- [Kingdom Come Deliverence](https://www.youtube.com/results?search_query=kingdom+come+deliverance)

##### Learning Materials
- [Shutter Effect](https://www.youtube.com/watch?v=pe4_Dimk7v0)
